function MyPromise(executor) {
  // this = {}
  // this.__proto__ = MyPromise.prototype
  // return this
  this.resolvedValue = null

  this.then = (callback) => {
    callback(this.resolvedValue)
  }

  this.resolve = (resolvedValue) => {
    this.resolvedValue = resolvedValue
  }

  executor(this.resolve)
}

//MyPromise.prototype.blah = "I exist on the MyPromise Prototype";

module.exports = MyPromise
