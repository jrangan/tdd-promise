const MyPromise = require('./my-promise')

// TODO test for not passing an executor

describe('my-promise', () => {
  it('MyPromise should be a constructor', () => {
    expect(typeof MyPromise).toBe('function')
  })
  it('MyPromise should have a then method', () => {
    const executor = () => {}
    const myPromise = new MyPromise(executor)
    expect(Object.getPrototypeOf(myPromise)).toBe(MyPromise.prototype)
    expect(typeof myPromise.then).toBe('function')
  })
  it('MyPromise should resolve with a value', (done) => {
    const promise = new MyPromise((resolve) => {
      resolve('Resolved value')
    })

    //callback passed to .then should be called
    const callback = (value) => {
      expect(value).toBe('Resolved value')
      done()
    }

    const wrappedCallback = jest.fn(callback)
    promise.then(wrappedCallback)

    expect(wrappedCallback).toHaveBeenCalled()
    //await expect(promise).resolves.toBe('Resolved value')
  })
})
